<?php

use PastellClient\Api\UserRolesRequester;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$userRolesRequester = new UserRolesRequester($client);


print_r("Get roles of user 2\n");
$listRoles = $userRolesRequester->all(2);
print_r($listRoles);

$firstRole = $listRoles[0];

print_r("Remove the first role from the user\n");
print_r($userRolesRequester->remove($firstRole));

print_r("Add the role back to the user\n");
print_r($userRolesRequester->add($firstRole));
