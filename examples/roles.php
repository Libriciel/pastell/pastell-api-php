<?php

use PastellClient\Api\RolesRequester;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$rolesRequester = new RolesRequester($client);


print_r("Get list of roles\n");
$listRoles = $rolesRequester->all();
print_r($listRoles);
