<?php

use PastellClient\Api\ConnectorsRequester;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$connectorsRequester = new ConnectorsRequester($client);


$connector = $connectorsRequester->create(ENTITY_ID, 'test', 'Test connector');
$connector->setLibelle('Updated test connector libelle');
$connector = $connectorsRequester->changeLabel($connector);


$connector->data->data['champs1'] = 'value1';
$connector = $connectorsRequester->update($connector);

$connector = $connectorsRequester->uploadFile(
    $connector,
    'champs5',
    __DIR__ . '/../tests/Api/fixtures/empty.pdf',
    'file.pdf'
);

$actionResult = $connectorsRequester->triggerAction($connector, 'ok');

print_r($actionResult);
