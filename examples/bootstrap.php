<?php

// phpcs:disable PSR1.Files.SideEffects
use Http\Adapter\Guzzle6\Client as GuzzleClient;
use PastellClient\Client;

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once __DIR__ . '/DefaultSettings.php';

function getDefaultPastellClient(): Client
{
    $client = new Client();
    $client->setUrl(URL);
    $client->authenticate(USERNAME, PASSWORD);
    return $client;
}

function getInsecurePastellClient(): Client
{
    $httpClient = GuzzleClient::createWithConfig(['verify' => false]);
    $client = Client::createWithHttpClient($httpClient);
    $client->setUrl(URL);
    $client->authenticate(USERNAME, PASSWORD);
    return $client;
}
