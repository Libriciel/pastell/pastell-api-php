<?php

use PastellClient\Api\Version;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$version = new Version($client);

print_r($version->show());
