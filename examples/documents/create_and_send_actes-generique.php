<?php

use PastellClient\Api\DocumentsRequester;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap.php';

$client = getInsecurePastellClient();
$documentsRequester = new DocumentsRequester($client);

$document = $documentsRequester->create(ENTITY_ID, 'actes-generique');

$document->documentData->data['acte_nature'] = 3;
$document->documentData->data['numero_de_lacte'] = '20200611';
$document->documentData->data['objet'] = 'Test Object';
$document->documentData->data['envoi_tdt'] = true;
$document->documentData->data['classification'] = '1.1';
$document = $documentsRequester->update($document);

$pathToFile = __DIR__ . '/../../tests/Api/fixtures/empty.pdf';
$document = $documentsRequester->uploadFile(
    $document,
    'arrete',
    $pathToFile,
    'empty.pdf'
);

$document = $documentsRequester->uploadFile(
    $document,
    'autre_document_attache',
    $pathToFile,
    'annex_1.pdf'
);

$document = $documentsRequester->uploadFile(
    $document,
    'autre_document_attache',
    $pathToFile,
    'annex_2.pdf',
    1
);

$document = $documentsRequester->setExternalData($document, 'type_piece', ['type_pj' => ['99_AI', '41_AT', '22_AT']]);

$tempFile = sys_get_temp_dir() . '/type_piece.json';
echo "Temp file : " . $tempFile . PHP_EOL;
file_put_contents($tempFile, $documentsRequester->getFile($document, 'type_piece_fichier'));

$actionResult = $documentsRequester->triggerAction($document, 'send-tdt');

print_r($actionResult);
