<?php

use PastellClient\Api\ModuleAssociationsRequester;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$moduleAssociations = new ModuleAssociationsRequester($client);


$associations = $moduleAssociations->all(ENTITY_ID);
print_r($associations);

$associate = $moduleAssociations->associate(ENTITY_ID, 250, 'actes-generique', 'TdT');
print_r($associate);

$disassociate = $moduleAssociations->disassociate(ENTITY_ID, $associate['id_fe']);

print_r($disassociate);
