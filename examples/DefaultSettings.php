<?php

if (! file_exists(__DIR__ . "/LocalSettings.php")) {
    echo <<<EOT
You need to create a LocalSettings.php file with these information :

<?php
define("URL","https://127.0.0.1:8443");
define("USERNAME","admin");
define("PASSWORD","admin");
define("ENTITY_ID", 12);
EOT;
    exit;
}

require_once __DIR__ . "/LocalSettings.php";
