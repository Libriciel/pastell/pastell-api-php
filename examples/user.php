<?php

use PastellClient\Api\UsersRequester;
use PastellClient\Model\User;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$usersRequester = new UsersRequester($client);


print_r("Get list of users\n");
$listUsers = $usersRequester->all();
print_r($listUsers);

print_r("Get one user\n");
$oneUser = $usersRequester->show($listUsers[0]->getId());
print_r($oneUser);

print_r("Create a new user\n");
$newUser = new User();
$newUser->login = time();
$newUser->nom = 'TEST';
$newUser->prenom = 'TEST';
$newUser->email = $newUser->login . '@example.org';
$newUser->password = 'test';

$createdUser = $usersRequester->create($newUser);
print_r($createdUser);

print_r("Update the user\n");
$createdUser->nom .= '_updated';
$updatedUser = $usersRequester->update($createdUser);
print_r($updatedUser);

print_r("Remove the user\n");
print_r($usersRequester->remove($updatedUser->getId()));
