<?php

use PastellClient\Api\EntitesRequester;
use PastellClient\Model\Entite;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$entiteApi = new EntitesRequester($client);

$listEntite = $entiteApi->all();
print_r($listEntite);

$oneEntite = $entiteApi->show($listEntite[0]->getId());
print_r($oneEntite);

$newEntite = new Entite();
$newEntite->denomination = time();
$newEntite->type = 'collectivite';
$newEntite->siren = '000000000';

$createdEntite = $entiteApi->create($newEntite);
print_r($createdEntite);

$createdEntite->denomination .= '_updated';

$updatedEntite = $entiteApi->update($createdEntite);
print_r($updatedEntite);

print_r($entiteApi->remove($updatedEntite->getId()));
