<?php

use PastellClient\Api\DocumentsRequester;

require_once __DIR__ . '/bootstrap.php';

$client = getInsecurePastellClient();
$documentsRequester = new DocumentsRequester($client);

$listDocuments = $documentsRequester->all(1);
print_r($listDocuments);

$oneDocument = $documentsRequester->show($listDocuments[0]->getIdE(), $listDocuments[0]->getIdD());
print_r($oneDocument);
