<?php

namespace PastellClient\Tests;

use GuzzleHttp\Psr7\Response;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\AddPathPlugin;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Message\Authentication\BasicAuth;
use Http\Message\Authentication\Bearer;
use PastellClient\Api\Version;
use PastellClient\Client;
use PastellClient\HttpClient\Builder;
use PastellClient\HttpClient\Plugin\PastellExceptionPlugin;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;

class ClientTest extends TestCase
{
    public function testShouldHaveGeneratedAnHttpClient()
    {
        $client = new Client();
        $this->assertInstanceOf(ClientInterface::class, $client->getHttpClient());
    }

    public function testShouldAddHostAndPathPlugin()
    {
        /**
         * @var Builder|MockObject $builderMock
         */
        $builderMock = $this->createMock(Builder::class);

        $builderMock->expects($this->exactly(2))
            ->method('removePlugin')
            ->withConsecutive(
                [AddHostPlugin::class],
                [AddPathPlugin::class]
            );

        $builderMock->expects($this->exactly(3))
            ->method('addPlugin')
            ->withConsecutive(
                [$this->isInstanceOf(PastellExceptionPlugin::class)],
                [$this->isInstanceOf(AddHostPlugin::class)],
                [$this->isInstanceOf(AddPathPlugin::class)]
            );

        $client = new Client(null, $builderMock);
        $client->setUrl('https://test.local');
    }

    public function testShouldAuthenticateTheUser()
    {
        /**
         * @var Builder|MockObject $builderMock
         */
        $builderMock = $this->createMock(Builder::class);

        $builderMock->expects($this->at(5))
            ->method('removePlugin')
            ->with(AuthenticationPlugin::class);

        $builderMock->expects($this->at(6))
            ->method('addPlugin')
            ->with(
                new AuthenticationPlugin(new BasicAuth('user', 'password'))
            );

        $client = new Client('https://url.tld', $builderMock);
        $client->authenticate('user', 'password');
    }
    public function testShouldAuthenticateTheUserWithToken(): void
    {
        /**
         * @var Builder|MockObject $builderMock
         */
        $builderMock = $this->createMock(Builder::class);

        $builderMock->expects($this->at(5))
            ->method('removePlugin')
            ->with(AuthenticationPlugin::class);

        $builderMock->expects($this->at(6))
            ->method('addPlugin')
            ->with(
                new AuthenticationPlugin(new Bearer('token'))
            );

        $client = new Client('https://url.tld', $builderMock);
        $client->tokenAuthenticate('token');
    }

    public function getApiClassesProvider(): iterable
    {
        yield ['version', Version::class];
    }

    /**
     * @dataProvider getApiClassesProvider
     * @param string $apiObject
     * @param string $fqcn
     */
    public function testGetApiClass(string $apiObject, string $fqcn)
    {
        $client = new Client();
        $this->assertInstanceOf($fqcn, $client->$apiObject());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testGet()
    {
        $httpClient = $this->createMock(HttpMethodsClientInterface::class);

        $expected = [
            'param1' => 'value1',
            'param2' => 'value2'
        ];
        $httpClient
            ->method('get')
            ->with('/version')
            ->willReturn(
                new Response(
                    200,
                    ['Content-Type' => 'application/json'],
                    json_encode($expected)
                )
            );
        /**
         * @var Client|MockObject $client
         */
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['getHttpClient'])
            ->getMock();
        $client->method('getHttpClient')->willReturn($httpClient);

        $this->assertSame(
            $expected,
            $client->get('/version')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $httpClient = $this->createMock(HttpMethodsClientInterface::class);

        $expected = [
            'status' => 'ok',
        ];
        $httpClient
            ->method('delete')
            ->with('/entite/1')
            ->willReturn(
                new Response(
                    200,
                    ['Content-Type' => 'application/json'],
                    json_encode($expected)
                )
            );
        /**
         * @var Client|MockObject $client
         */
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['getHttpClient'])
            ->getMock();
        $client->method('getHttpClient')->willReturn($httpClient);

        $this->assertSame(
            $expected,
            $client->delete('/entite/1')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testPost()
    {
        $httpClient = $this->createMock(HttpMethodsClientInterface::class);

        $expected = [
            'param1' => 'value1',
            'param2' => 'value2'
        ];
        $httpClient
            ->method('post')
            ->with('/entite')
            ->willReturn(
                new Response(
                    200,
                    ['Content-Type' => 'application/json'],
                    json_encode($expected)
                )
            );
        /**
         * @var Client|MockObject $client
         */
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['getHttpClient'])
            ->getMock();
        $client->method('getHttpClient')->willReturn($httpClient);

        $this->assertSame(
            $expected,
            $client->post('/entite', $expected)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testPatch()
    {
        $httpClient = $this->createMock(HttpMethodsClientInterface::class);

        $expected = [
            'param1' => 'value1',
            'param2' => 'value2'
        ];
        $httpClient
            ->method('patch')
            ->with('/entite/1')
            ->willReturn(
                new Response(
                    200,
                    ['Content-Type' => 'application/json'],
                    json_encode($expected)
                )
            );
        /**
         * @var Client|MockObject $client
         */
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['getHttpClient'])
            ->getMock();
        $client->method('getHttpClient')->willReturn($httpClient);

        $this->assertSame(
            $expected,
            $client->patch('/entite/1', $expected)
        );
    }
}
