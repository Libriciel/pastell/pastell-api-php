<?php

namespace PastellClient\Tests\HttpClient\Plugin;

use GuzzleHttp\Psr7\Response;
use Http\Promise\FulfilledPromise;
use Http\Promise\Promise;
use PastellClient\Exception\ForbiddenException;
use PastellClient\Exception\NotFoundException;
use PastellClient\Exception\PastellException;
use PastellClient\Exception\UnauthorizedException;
use PastellClient\Exception\UnexpectedException;
use PastellClient\HttpClient\Plugin\PastellExceptionPlugin;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PastellExceptionPluginTest extends TestCase
{
    public function handleRequestProvider(): iterable
    {
        yield '200' => [
            'response' => new Response(200),
            'exception' => null
        ];

        yield '201' => [
            'response' => new Response(201),
            'exception' => null
        ];

        yield '400' => [
            'response' => new Response(
                400,
                [],
                json_encode([
                    'status' => 'error',
                    'error-message' => "Ce connecteur n'existe pas"
                ])
            ),
            'exception' => new PastellException(
                json_encode([
                    'status' => 'error',
                    'error-message' => "Ce connecteur n'existe pas"
                ]),
                400
            )
        ];

        yield '401' => [
            'response' => new Response(
                401,
                [],
                json_encode([
                    'status' => 'error',
                    'error-message' => 'Accès interdit'
                ])
            ),
            'exception' => new UnauthorizedException(
                json_encode([
                    'status' => 'error',
                    'error-message' => 'Accès interdit'
                ]),
                401
            ),
        ];

        yield '403' => [
            'response' => new Response(
                403,
                [],
                json_encode([
                    'status' => 'error',
                    'error-message' => 'Acces interdit id_e=2, droit=entite:lecture,id_u=3'
                ])
            ),
            'exception' => new ForbiddenException(
                json_encode([
                    'status' => 'error',
                    'error-message' => 'Acces interdit id_e=2, droit=entite:lecture,id_u=3'
                ]),
                403
            ),
        ];

        yield '404' => [
            'response' => new Response(
                404,
                [],
                json_encode([
                    'status' => 'error',
                    'error-message' => "L'entité 1 n'a pas été trouvée"
                ])
            ),
            'exception' => new NotFoundException(
                json_encode([
                    'status' => 'error',
                    'error-message' => "L'entité 1 n'a pas été trouvée"
                ]),
                404
            ),
        ];

        yield '409' => [
            'response' => new Response(
                409,
                [],
                json_encode([
                    'status' => 'error',
                    'error-message' => "L'extension test est déjà présente"
                ])
            ),
            'exception' => new PastellException(
                json_encode([
                    'status' => 'error',
                    'error-message' => "L'extension test est déjà présente"
                ]),
                409
            )
        ];

        yield '503' => [
            'response' => new Response(503, [], 'Service Unavailable'),
            'exception' => new UnexpectedException('Service Unavailable', 503)
        ];
    }

    /**
     * @dataProvider handleRequestProvider
     * @param ResponseInterface $response
     * @param \Exception|null $exception
     * @throws \Exception
     */
    public function testHandleRequest(ResponseInterface $response, \Exception $exception = null)
    {
        /** @var RequestInterface|MockObject $requestMock */
        $requestMock = $this->createMock(RequestInterface::class);
        $promiseMock = $this->createMock(Promise::class);
        $promiseMock->expects($this->once())
            ->method('then')
            ->willReturnCallback(function ($callback) use ($response) {
                return new FulfilledPromise($callback($response));
            });

        $plugin = new PastellExceptionPlugin();
        if ($exception !== null) {
            $this->expectException(get_class($exception));
            $this->expectExceptionCode($exception->getCode());
            $this->expectExceptionMessage($exception->getMessage());
        }
        $fulfilledPromise = $plugin->handleRequest(
            $requestMock,
            function (RequestInterface $request) use ($promiseMock) {
                return $promiseMock;
            },
            function (RequestInterface $request) use ($promiseMock) {
                return $promiseMock;
            }
        );

        $this->assertSame($response, $fulfilledPromise->wait());
    }
}
