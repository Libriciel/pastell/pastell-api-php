<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\Version;
use PastellClient\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class VersionTest extends TestCase
{
    /**
     * @throws ClientExceptionInterface
     */
    public function testGetVersion()
    {
        $expected = [
            'version' => '3.0.1',
            'revision' => 45094,
            'last_changed_date' => 'lundi 18 novembre 2019, 13:58:06 (UTC+0000)',
            'extensions_versions_accepted' => [
                '3.0.0'
            ],
            'version_complete' => 'Version 3.0.1 - Révision  45094'
        ];

        /**
         * @var Client|MockObject $clientMock
         */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(Version::VERSION_PATH)
            ->willReturn($expected);

        $version = new Version($clientMock);
        $this->assertSame($expected, $version->show());
    }
}
