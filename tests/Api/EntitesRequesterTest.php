<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\EntitesRequester;
use PastellClient\Client;
use PastellClient\Hydrator\EntiteHydrator;
use PastellClient\Model\Entite;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class EntitesRequesterTest extends TestCase
{
    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->entiteHydrator = new EntiteHydrator();
    }
    /**
     * @throws ClientExceptionInterface
     */
    public function testGetAllEntites()
    {
        $expected = [
            [
                'id_e' => '1',
                'denomination' => 'Entity Name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'centre_de_gestion' => '0',
                'entite_mere' => '0'
            ]
        ];
        $hydrated = [];
        foreach ($expected as $entite) {
            $hydrated[] = $this->entiteHydrator->hydrate($entite);
        }

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(EntitesRequester::ENTITE_PATH)
            ->willReturn($expected);

        $entite = new EntitesRequester($clientMock);
        $this->assertEquals($hydrated, $entite->all());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testGetOneEntite()
    {
        $expected = [
            'id_e' => '1',
            'denomination' => 'Entity Name',
            'siren' => '000000000',
            'type' => 'collectivite',
            'entite_mere' => '0',
            'entite_fille' => [],
            'centre_de_gestion' => '0',
        ];

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(EntitesRequester::ENTITE_PATH . '/1')
            ->willReturn($expected);

        $entite = new EntitesRequester($clientMock);
        $this->assertEquals($this->entiteHydrator->hydrate($expected), $entite->show(1));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testRemoveEntite()
    {
        $expected = [
            'result' => 'ok'
        ];

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('delete')
            ->with(EntitesRequester::ENTITE_PATH . '/1')
            ->willReturn($expected);

        $entite = new EntitesRequester($clientMock);
        $this->assertSame($expected, $entite->remove(1));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testCreateEntite()
    {
        $expected = [
            'id_e' => '1',
            'denomination' => 'Entity Name',
            'siren' => '000000000',
            'type' => 'collectivite',
            'entite_mere' => '0',
            'entite_fille' => [],
            'centre_de_gestion' => '0',
        ];

        $entite = new Entite();
        $entite->denomination = 'Entity Name';
        $entite->siren = '000000000';
        $entite->type = 'collectivite';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('post')
            ->with(EntitesRequester::ENTITE_PATH)
            ->willReturn($expected);

        $entiteApi = new EntitesRequester($clientMock);
        $this->assertEquals($this->entiteHydrator->hydrate($expected), $entiteApi->create($entite));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testEditEntity()
    {
        $expected = [
            'id_e' => '1',
            'denomination' => 'Entity Name Update',
            'siren' => '000000000',
            'type' => 'collectivite',
            'entite_mere' => '0',
            'entite_fille' => [],
            'centre_de_gestion' => '0',
        ];

        $entite = $this->entiteHydrator->hydrate($expected);
        $entite->denomination = 'Entity Name Update';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('patch')
            ->with(EntitesRequester::ENTITE_PATH . '/1')
            ->willReturn($expected);

        $entiteApi = new EntitesRequester($clientMock);
        $this->assertEquals($this->entiteHydrator->hydrate($expected), $entiteApi->update($entite));
    }
}
