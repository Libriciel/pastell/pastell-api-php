<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\UsersRequester;
use PastellClient\Client;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class UsersRequesterTest extends TestCase
{
    /**
     * @var UserHydrator
     */
    private $userHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->userHydrator = new UserHydrator();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAll()
    {
        $expected = [
            [
                'id_u' => '1',
                'login' => 'admin',
                'email' => 'admin@example.org',
            ],
            [
                'id_u' => '2',
                'login' => 'test',
                'email' => 'test@example.org',
            ]
        ];
        $hydrated = [];
        foreach ($expected as $users) {
            $hydrated[] = $this->userHydrator->hydrate($users);
        }

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(UsersRequester::USER_PATH)
            ->willReturn($expected);

        $user = new UsersRequester($clientMock);
        $this->assertEquals($hydrated, $user->all());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testShow()
    {
        $expected = [
            'id_u' => '1',
            'login' => 'admin',
            'nom' => 'admin',
            'prenom' => 'admin',
            'email' => 'admin@example.org',
            'certificat' => '',
            'id_e' => '0'
        ];

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(UsersRequester::USER_PATH . '/1')
            ->willReturn($expected);

        $user = new UsersRequester($clientMock);
        $this->assertEquals($this->userHydrator->hydrate($expected), $user->show(1));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testCreate()
    {
        $expected = [
            'id_u' => '2',
            'login' => 'test',
            'nom' => 'test',
            'prenom' => 'test',
            'email' => 'test@example.org',
            'certificat' => '',
            'id_e' => '0'
        ];

        $user = new User();
        $user->login = 'test';
        $user->nom = 'test';
        $user->prenom = 'test';
        $user->email = 'test@example.org';
        $user->password = 'test';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('post')
            ->with(UsersRequester::USER_PATH)
            ->willReturn($expected);

        $usersRequester = new UsersRequester($clientMock);
        $this->assertEquals($this->userHydrator->hydrate($expected), $usersRequester->create($user));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        $expected = [
            'id_u' => '2',
            'login' => 'test',
            'nom' => 'test updated',
            'prenom' => 'test',
            'email' => 'test@example.org',
            'certificat' => '',
            'id_e' => '0',
            'result' => 'ok'
        ];

        $user = $this->userHydrator->hydrate($expected);
        $user->nom = 'test updated';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('patch')
            ->with(UsersRequester::USER_PATH . '/2')
            ->willReturn($expected);

        $usersRequester = new UsersRequester($clientMock);
        $this->assertEquals($this->userHydrator->hydrate($expected), $usersRequester->update($user));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testRemove()
    {
        $expected = [
            'result' => 'ok'
        ];

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('delete')
            ->with(UsersRequester::USER_PATH . '/2')
            ->willReturn($expected);

        $user = new UsersRequester($clientMock);
        $this->assertSame($expected, $user->remove(2));
    }
}
