<?php

namespace PastellClient\Tests\Api;

use Http\Client\Exception;
use Http\Factory\Guzzle\StreamFactory;
use PastellClient\Api\ConnectorsRequester;
use PastellClient\Client;
use PastellClient\Hydrator\ConnectorHydrator;
use PastellClient\Model\ActionResult;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class ConnectorsRequesterTest extends TestCase
{
    /** @var ConnectorHydrator */
    private $connectorHydrator;

    /** @var Client|MockObject */
    private $clientMock;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->connectorHydrator = new ConnectorHydrator();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->clientMock = $this->createMock(Client::class);
    }

    public function tearDown(): void
    {
        $this->clientMock = null;
        parent::tearDown();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAll()
    {
        $expected = [
            [
                'id_ce' => '12',
                'id_e' => '1',
                'libelle' => 'label',
                'id_connecteur' => 's2low',
                'type' => 'TdT',
                'frequence_en_minute' => '1',
                'id_verrou' => '',
            ]
        ];
        $hydrated = [];
        foreach ($expected as $connector) {
            $hydrated[] = $this->connectorHydrator->hydrate($connector);
        }

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(ConnectorsRequester::ALL_CONNECTOR_PATH)
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals($hydrated, $connectorsRequester->all());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAllByType()
    {
        $type = 's2low';
        $expected = [
            [
                'id_ce' => '12',
                'id_e' => '1',
                'libelle' => 'label',
                'id_connecteur' => 's2low',
                'type' => 'TdT',
                'frequence_en_minute' => '1',
                'id_verrou' => '',
            ]
        ];
        $hydrated = [];
        foreach ($expected as $connector) {
            $hydrated[] = $this->connectorHydrator->hydrate($connector);
        }

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(ConnectorsRequester::ALL_CONNECTOR_PATH . "/$type")
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals($hydrated, $connectorsRequester->allByType($type));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testShow()
    {
        $expected = [
            'id_ce' => '12',
            'id_e' => '1',
            'libelle' => 'label',
            'id_connecteur' => 's2low',
            'type' => 'TdT',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
            'data' => [
                'url' => 'https://s2low',
                'user_login' => 'login',
            ],
        ];

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/' . 12)
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $this->connectorHydrator->hydrate($expected),
            $connectorsRequester->show(1, 12)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testCreate()
    {
        $expected = [
            'id_ce' => '12',
            'id_e' => '1',
            'libelle' => 'label',
            'id_connecteur' => 's2low',
            'type' => 'TdT',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
        ];

        $this->clientMock
            ->expects($this->once())
            ->method('post')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1))
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $this->connectorHydrator->hydrate($expected),
            $connectorsRequester->create(1, 's2low', 'label')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        $expected = [
            'id_ce' => '12',
            'id_e' => '1',
            'libelle' => 'label',
            'id_connecteur' => 's2low',
            'type' => 'TdT',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
            'data' => [
                'url' => 'https://s2low',
                'user_login' => 'login',
            ],
        ];

        $connector = $this->connectorHydrator->hydrate($expected);

        $this->clientMock->expects($this->once())
            ->method('patch')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/' . 12 . '/content')
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $this->connectorHydrator->hydrate($expected),
            $connectorsRequester->update($connector)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testChangeLabel()
    {
        $expected = [
            'id_ce' => '12',
            'id_e' => '1',
            'libelle' => 'label',
            'id_connecteur' => 's2low',
            'type' => 'TdT',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
            'data' => [
                'url' => 'https://s2low',
                'user_login' => 'login',
            ],
        ];

        $connector = $this->connectorHydrator->hydrate($expected);
        $newLabel = 'new label';
        $connector->setLibelle($newLabel);
        $expected['libelle'] = $newLabel;

        $this->clientMock->expects($this->once())
            ->method('patch')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/' . 12)
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $this->connectorHydrator->hydrate($expected),
            $connectorsRequester->changeLabel($connector)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testGetExternalData()
    {
        $expected = [
            'Actes',
            'PADES',
            'PES'
        ];

        $connector = $this->connectorHydrator->hydrate([
            'id_ce' => '27',
            'id_e' => '1',
            'libelle' => 'iParapheur',
            'id_connecteur' => 'iparapheur',
            'type' => 'signature',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
        ]);

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(
                sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, $connector->getIdE())
                . sprintf('/%s/externalData/%s', $connector->getIdCe(), 'iparapheur_type')
            )
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $expected,
            $connectorsRequester->getExternalData($connector, 'iparapheur_type')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testSetExternalData()
    {
        $expected = [
            'id_ce' => '27',
            'id_e' => '1',
            'libelle' => 'iParapheur',
            'id_connecteur' => 'iparapheur',
            'type' => 'signature',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
            'data' => [
                'iparapheur_type' => 'Actes',
            ],
        ];
        $connector = $this->connectorHydrator->hydrate($expected);
        $this->clientMock
            ->expects($this->once())
            ->method('patch')
            ->with(
                sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, $connector->getIdE())
                . sprintf('/%s/externalData/%s', $connector->getIdCe(), 'iparapheur_type')
            )
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $connector,
            $connectorsRequester->setExternalData($connector, 'iparapheur_type', ['iparapheur_type' => 'Actes'])
        );
    }

    /**
     * @throws Exception
     */
    public function testGetFile(): void
    {
        $streamFactory = new StreamFactory();
        $expected = $streamFactory->createStreamFromFile(__DIR__ . '/fixtures/empty.pdf');

        $connector = $this->connectorHydrator->hydrate(
            [
                'id_ce' => '12',
                'id_e' => '1',
                'libelle' => 'label',
                'id_connecteur' => 's2low',
                'type' => 'TdT',
                'frequence_en_minute' => '1',
                'id_verrou' => '',
                'data' => [
                    'url' => 'https://s2low',
                    'user_login' => 'login',
                ],
            ]
        );

        $this->clientMock
            ->expects($this->once())
            ->method('getAsStream')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/12/file/user_certificat')
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertStringEqualsFile(
            __DIR__ . '/fixtures/empty.pdf',
            $connectorsRequester->getFile($connector, 'user_certificat')->getContents()
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUploadFile(): void
    {
        $expected = [
            'id_ce' => '12',
            'id_e' => '1',
            'libelle' => 'label',
            'id_connecteur' => 's2low',
            'type' => 'TdT',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
            'data' => [
                'url' => 'https://s2low',
                'user_login' => 'login',
            ],
        ];

        $connector = $this->connectorHydrator->hydrate($expected);

        $this->clientMock->expects($this->once())
            ->method('post')
            ->with(sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/12/file/user_certificat')
            ->willReturn($expected);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            $this->connectorHydrator->hydrate($expected),
            $connectorsRequester->uploadFile(
                $connector,
                'user_certificat',
                __DIR__ . '/fixtures/empty.pdf',
                'empty.p12'
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testTriggerAction()
    {
        $expected = [
            'id_ce' => '27',
            'id_e' => '1',
            'libelle' => 'iParapheur',
            'id_connecteur' => 'iparapheur',
            'type' => 'signature',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
        ];

        $connector = $this->connectorHydrator->hydrate($expected);

        $this->clientMock
            ->expects($this->once())
            ->method('post')
            ->with(
                sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1)
                . sprintf('/%s/action/%s', $connector->getIdCe(), 'test-iparapheur')
            )
            ->willReturn([
                'result' => true,
                'last_message' => 'La connexion est réussie : [ws@pastell] m\'a dit: "Dès Noël où un zéphyr haï me'
            ]);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertEquals(
            new ActionResult(true, 'La connexion est réussie : [ws@pastell] m\'a dit: "Dès Noël où un zéphyr haï me'),
            $connectorsRequester->triggerAction($connector, 'test-iparapheur')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $connector = $this->connectorHydrator->hydrate([
            'id_ce' => '27',
            'id_e' => '1',
            'libelle' => 'iParapheur',
            'id_connecteur' => 'iparapheur',
            'type' => 'signature',
            'frequence_en_minute' => '1',
            'id_verrou' => '',
        ]);
        $this->clientMock
            ->expects($this->once())
            ->method('delete')
            ->with(
                sprintf(ConnectorsRequester::ENTITY_CONNECTOR_PATH, 1) . '/' . $connector->getIdCe()
            )
            ->willReturn([
                'result' => 'ok',
            ]);

        $connectorsRequester = new ConnectorsRequester($this->clientMock);
        $this->assertTrue($connectorsRequester->delete($connector));
    }
}
