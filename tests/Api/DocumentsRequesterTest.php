<?php

namespace PastellClient\Tests\Api;

use Http\Client\Exception;
use Http\Factory\Guzzle\StreamFactory;
use PastellClient\Api\DocumentsRequester;
use PastellClient\Client;
use PastellClient\Hydrator\DocumentHydrator;
use PastellClient\Model\ActionResult;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class DocumentsRequesterTest extends TestCase
{
    public const ENTITY_ID = 1;
    public const DOCUMENT_ID = 'RQ5MHWh';

    /** @var DocumentHydrator */
    private $documentHydrator;

    /** @var Client|MockObject */
    private $clientMock;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->documentHydrator = new DocumentHydrator();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->clientMock = $this->createMock(Client::class);
    }

    public function tearDown(): void
    {
        $this->clientMock = null;
        parent::tearDown();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAll()
    {
        $expected = [
            [
                'id_d' => self::DOCUMENT_ID,
                'id_e' => '1',
                'type' => 'actes-automatique',
                'last_action' => 'termine',
                'last_action_date' => '2020-04-01 17:08:00',
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ]
        ];
        $hydrated = [];
        foreach ($expected as $document) {
            $hydrated[] = $this->documentHydrator->hydrate($document);
        }

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID))
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals($hydrated, $documentRequester->all(self::ENTITY_ID));
    }

    public function documentTypeProvider(): iterable
    {
        yield [
            'type' => 'unknown_type',
            'data' => [
                'to' => 'example@example.org',
                'cc' => '',
                'bcc' => '',
                'password' => 'MOT DE PASSE NON RECUPERABLE',
                'password2' => 'MOT DE PASSE NON RECUPERABLE',
                'objet' => 'objet',
                'message' => '',
            ],
        ];

        yield [
            'type' => 'mailsec',
            'data' => [
                'to' => 'example@example.org',
                'cc' => '',
                'bcc' => '',
                'password' => 'MOT DE PASSE NON RECUPERABLE',
                'password2' => 'MOT DE PASSE NON RECUPERABLE',
                'objet' => 'objet',
                'message' => '',
            ],
        ];
    }

    /**
     * @dataProvider documentTypeProvider
     * @throws ClientExceptionInterface
     */
    public function testGetCustomDocument(string $type, array $data)
    {
        $expected = [
            'info' => [
                'id_d' => self::DOCUMENT_ID,
                'type' => $type,
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ],
            'data' => $data,
            'last_action' => [
                'action' => 'modification',
                'message' => 'Modification du document',
                'date' => '2020-04-01 17:08:00',
            ],
        ];

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID) . '/' . self::DOCUMENT_ID)
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $this->documentHydrator->hydrateFromDocumentDetail($expected, self::ENTITY_ID),
            $documentRequester->show(self::ENTITY_ID, self::DOCUMENT_ID)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testCreateDocument()
    {
        $expected = [
            'info' => [
                'id_d' => self::DOCUMENT_ID,
                'type' => 'actes-generique',
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ],
            'data' => [
                'date_de_lacte' => '2020-04-14',
            ],
            'last_action' => [
                'action' => 'modification',
                'message' => 'Modification du document',
                'date' => '2020-04-01 17:08:00',
            ],
        ];

        $this->clientMock
            ->expects($this->once())
            ->method('post')
            ->with(sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID))
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $this->documentHydrator->hydrateFromDocumentDetail($expected, self::ENTITY_ID),
            $documentRequester->create(self::ENTITY_ID, 'actes-generique')
        );
    }

    /**
     * @dataProvider documentTypeProvider
     * @throws ClientExceptionInterface
     */
    public function testUpdateDocument(string $type, array $data)
    {
        $expected = [
            'content' => [
                'info' => [
                    'id_d' => self::DOCUMENT_ID,
                    'type' => $type,
                    'creation' => '2020-04-01 17:08:00',
                    'modification' => '2020-04-01 17:08:00',
                ],
                'data' => $data,
                'last_action' => [
                    'action' => 'modification',
                    'message' => 'Modification du document',
                    'date' => '2020-04-01 17:08:00',
                ],
            ]
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail($expected['content'], self::ENTITY_ID);

        $this->clientMock->expects($this->once())
            ->method('patch')
            ->with(sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID) . '/' . self::DOCUMENT_ID)
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $this->documentHydrator->hydrateFromDocumentDetail($expected['content'], self::ENTITY_ID),
            $documentRequester->update($document)
        );
    }

    /**
     * @throws Exception
     */
    public function testGetFile(): void
    {
        $streamFactory = new StreamFactory();
        $expected = $streamFactory->createStreamFromFile(__DIR__ . '/fixtures/empty.pdf');

        $document = $this->documentHydrator->hydrateFromDocumentDetail(
            [
                'info' => [
                    'id_d' => self::DOCUMENT_ID,
                    'type' => 'actes-generique',
                    'creation' => '2020-04-01 17:08:00',
                    'modification' => '2020-04-01 17:08:00',
                ],
                'data' => [
                    'date_de_lacte' => '2020-04-14',
                ],
                'last_action' => [
                    'action' => 'modification',
                    'message' => 'Modification du document',
                    'date' => '2020-04-01 17:08:00',
                ],
            ],
            self::ENTITY_ID
        );

        $this->clientMock
            ->expects($this->once())
            ->method('getAsStream')
            ->with(sprintf(DocumentsRequester::DOCUMENT_FILE_PATH, self::ENTITY_ID, self::DOCUMENT_ID, 'arrete', 0))
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertStringEqualsFile(
            __DIR__ . '/fixtures/empty.pdf',
            $documentRequester->getFile($document, 'arrete')->getContents()
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUploadFile()
    {
        $expected = [
            'content' => [
                'info' => [
                    'id_d' => self::DOCUMENT_ID,
                    'type' => 'actes-generique',
                    'creation' => '2020-04-01 17:08:00',
                    'modification' => '2020-04-01 17:08:00',
                ],
                'data' => [
                    'date_de_lacte' => '2020-04-14',
                ],
                'last_action' => [
                    'action' => 'modification',
                    'message' => 'Modification du document',
                    'date' => '2020-04-01 17:08:00',
                ],
            ]
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail($expected['content'], self::ENTITY_ID);

        $this->clientMock
            ->expects($this->once())
            ->method('post')
            ->with(sprintf(DocumentsRequester::DOCUMENT_FILE_PATH, self::ENTITY_ID, self::DOCUMENT_ID, 'arrete', 0))
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $document,
            $documentRequester->uploadFile($document, 'arrete', __DIR__ . '/fixtures/empty.pdf', 'empty.pdf')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDeleteFile()
    {
        $expected = [
            'info' => [
                'id_d' => self::DOCUMENT_ID,
                'type' => 'actes-generique',
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ],
            'data' => [
                'date_de_lacte' => '2020-04-14',
            ],
            'last_action' => [
                'action' => 'modification',
                'message' => 'Modification du document',
                'date' => '2020-04-01 17:08:00',
            ],
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail($expected, self::ENTITY_ID);

        $this->clientMock
            ->expects($this->once())
            ->method('delete')
            ->with(sprintf(DocumentsRequester::DOCUMENT_FILE_PATH, self::ENTITY_ID, self::DOCUMENT_ID, 'arrete', 0))
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $document,
            $documentRequester->deleteFile($document, 'arrete')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testTriggerAction()
    {
        $expected = [
            'info' => [
                'id_d' => self::DOCUMENT_ID,
                'type' => 'actes-generique',
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ],
            'data' => [
                'date_de_lacte' => '2020-04-14',
            ],
            'last_action' => [
                'action' => 'modification',
                'message' => 'Modification du document',
                'date' => '2020-04-01 17:08:00',
            ],
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail($expected, self::ENTITY_ID);

        $this->clientMock
            ->expects($this->once())
            ->method('post')
            ->with(
                sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID)
                . sprintf('/%s/action/%s', $document->getIdD(), 'send-ged')
            )
            ->willReturn([
                'result' => true,
                'message' => 'Le dossier HELIOS_SIMU_ALR2_1571902731_858370460.xml a été versé sur le dépôt'
            ]);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            new ActionResult(true, 'Le dossier HELIOS_SIMU_ALR2_1571902731_858370460.xml a été versé sur le dépôt'),
            $documentRequester->triggerAction($document, 'send-ged')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testGetExternalData()
    {
        $expected = [
            'actes_type_pj_list' => [
                '99_AI' => 'Acte individuel (99_AI)',
            ],
            'pieces' => [
                'PA-DOC-API.pdf'
            ]
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail(
            [
                'info' => [
                    'id_d' => self::DOCUMENT_ID,
                    'type' => 'actes-generique',
                    'creation' => '2020-04-01 17:08:00',
                    'modification' => '2020-04-01 17:08:00',
                ],
                'data' => [
                    'date_de_lacte' => '2020-04-14',
                ],
                'last_action' => [
                    'action' => 'modification',
                    'message' => 'Modification du document',
                    'date' => '2020-04-01 17:08:00',
                ],
            ],
            self::ENTITY_ID
        );

        $this->clientMock
            ->expects($this->once())
            ->method('get')
            ->with(
                sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID)
                . sprintf('/%s/externalData/%s', $document->getIdD(), 'type_piece')
            )
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $expected,
            $documentRequester->getExternalData($document, 'type_piece')
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testSetExternalData()
    {
        $expected = [
            'info' => [
                'id_d' => self::DOCUMENT_ID,
                'type' => 'actes-generique',
                'creation' => '2020-04-01 17:08:00',
                'modification' => '2020-04-01 17:08:00',
            ],
            'data' => [
                'date_de_lacte' => '2020-04-14',
                'type_acte' => '99_AI',
            ],
            'last_action' => [
                'action' => 'modification',
                'message' => 'Modification du document',
                'date' => '2020-04-01 17:08:00',
            ],
        ];

        $document = $this->documentHydrator->hydrateFromDocumentDetail($expected, self::ENTITY_ID);

        $this->clientMock
            ->expects($this->once())
            ->method('patch')
            ->with(
                sprintf(DocumentsRequester::DOCUMENT_PATH, self::ENTITY_ID)
                . sprintf('/%s/externalData/%s', $document->getIdD(), 'type_piece')
            )
            ->willReturn($expected);

        $documentRequester = new DocumentsRequester($this->clientMock);
        $this->assertEquals(
            $document,
            $documentRequester->setExternalData($document, 'type_piece', ['type_piece' => ['99_AI']])
        );
    }
}
