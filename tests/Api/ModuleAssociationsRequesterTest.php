<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\ModuleAssociationsRequester;
use PastellClient\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class ModuleAssociationsRequesterTest extends TestCase
{
    public function getAllProvider(): iterable
    {
        yield [null, null, ''];
        yield ['actes-generique', null, '?flux=actes-generique'];
        yield [null, 'TdT', '?type=TdT'];
        yield ['actes-generique', 'TdT', '?flux=actes-generique&type=TdT'];
    }

    /**
     * @dataProvider getAllProvider
     * @throws ClientExceptionInterface
     */
    public function testGetAll(?string $module, ?string $type, string $expectedQueryParams)
    {
        $expected = [
            [
                'id_fe' => 306,
                'id_e' => 104,
                'flux' => 'actes-generique',
                'id_ce' => 250,
                'type' => 'TdT',
                'num_same_type' => 0,
            ]
        ];

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock
            ->expects($this->once())
            ->method('get')
            ->with(sprintf(ModuleAssociationsRequester::ENTITY_MODULE_PATH, 1) . $expectedQueryParams)
            ->willReturn($expected);

        $moduleAssociation = new ModuleAssociationsRequester($clientMock);
        $this->assertSame($expected, $moduleAssociation->all(1, $module, $type));
    }

    public function testAssociate()
    {
        $expected = [
            'id_fe' => 307,
        ];
        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock
            ->expects($this->once())
            ->method('post')
            ->with(
                sprintf(ModuleAssociationsRequester::ENTITY_MODULE_PATH, 1)
                . sprintf('/%s/connecteur/%s', 'actes-generique', 250)
            )
            ->willReturn($expected);

        $moduleAssociation = new ModuleAssociationsRequester($clientMock);
        $this->assertSame($expected, $moduleAssociation->associate(1, 250, 'actes-generique', 'TdT'));
    }

    public function testDisassociate()
    {
        $expected = [
            'result' => 'ok',
        ];
        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock
            ->expects($this->once())
            ->method('delete')
            ->with(
                sprintf(ModuleAssociationsRequester::ENTITY_MODULE_PATH, 1) . '?id_fe=306'
            )
            ->willReturn($expected);

        $moduleAssociation = new ModuleAssociationsRequester($clientMock);
        $this->assertSame($expected, $moduleAssociation->disassociate(1, 306));
    }
}
