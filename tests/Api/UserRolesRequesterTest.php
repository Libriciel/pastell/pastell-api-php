<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\UserRolesRequester;
use PastellClient\Client;
use PastellClient\Hydrator\UserRoleHydrator;
use PastellClient\Model\UserRole;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class UserRolesRequesterTest extends TestCase
{
    /**
     * @var UserRoleHydrator
     */
    private $userRoleHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->userRoleHydrator = new UserRoleHydrator();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAll()
    {
        $expected = [
            [
                'id_u' => 1,
                'id_e' => 1,
                'role' => 'admin',
                'droits' => [
                    'system:lecture',
                    'system:edition'
                ]
            ],
            [
                'id_u' => 1,
                'id_e' => 1,
                'role' => 'actes',
                'droits' => [
                    'actes-generique:lecture',
                    'actes-generique:edition'
                ]
            ]
        ];
        $hydrated = [];
        foreach ($expected as $userRoles) {
            $hydrated[] = $this->userRoleHydrator->hydrate($userRoles);
        }

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(sprintf(UserRolesRequester::USER_ROLES_PATH, 1))
            ->willReturn($expected);

        $userRolesRequester = new UserRolesRequester($clientMock);
        $this->assertEquals($hydrated, $userRolesRequester->all(1));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAddRole()
    {
        $expected = [
            'result' => 'ok'
        ];

        $userRole = new UserRole();
        $userRole->id_u = 1;
        $userRole->id_e = 2;
        $userRole->role = 'test';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('post')
            ->with(sprintf(UserRolesRequester::USER_ROLES_PATH, 1))
            ->willReturn($expected);

        $userRolesRequester = new UserRolesRequester($clientMock);
        $this->assertEquals($expected, $userRolesRequester->add($userRole));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testRemoveRole()
    {
        $expected = [
            'result' => 'ok'
        ];

        $userRole = new UserRole();
        $userRole->id_u = 1;
        $userRole->id_e = 2;
        $userRole->role = 'test';

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('delete')
            ->with(sprintf(UserRolesRequester::USER_ROLES_PATH, 1) . '?' . http_build_query($userRole))
            ->willReturn($expected);

        $userRolesRequester = new UserRolesRequester($clientMock);
        $this->assertEquals($expected, $userRolesRequester->remove($userRole));
    }
}
