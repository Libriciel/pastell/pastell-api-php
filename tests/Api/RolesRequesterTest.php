<?php

namespace PastellClient\Tests\Api;

use PastellClient\Api\RolesRequester;
use PastellClient\Client;
use PastellClient\Hydrator\RoleHydrator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class RolesRequesterTest extends TestCase
{
    /**
     * @var RoleHydrator
     */
    private $roleHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->roleHydrator = new RoleHydrator();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testGetAllRoles()
    {
        $expected = [
            [
                'role' => 'admin',
                'libelle' => 'Administrator'
            ],
            [
                'role' => 'studio',
                'libelle' => 'Studio permissions'
            ],

        ];
        $hydrated = [];
        foreach ($expected as $role) {
            $hydrated[] = $this->roleHydrator->hydrate($role);
        }

        /** @var Client|MockObject $clientMock */
        $clientMock = $this->createMock(Client::class);
        $clientMock->expects($this->once())
            ->method('get')
            ->with(RolesRequester::ROLE_PATH)
            ->willReturn($expected);

        $rolesRequester = new RolesRequester($clientMock);
        $this->assertEquals($hydrated, $rolesRequester->all());
    }
}
