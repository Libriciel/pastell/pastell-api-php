FROM php:7.2-cli

WORKDIR /app

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN apt-get -y update && apt-get install -y --no-install-recommends \
    git \
    zip \
    zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN  docker-php-ext-install zip

RUN pecl install xdebug-3.1.6 && docker-php-ext-enable xdebug

COPY . /app

