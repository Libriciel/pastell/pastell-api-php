<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\Entite;

class EntiteHydrator implements Hydrator
{
    /**
     * @param array $data
     * @return Entite
     */
    public function hydrate(array $data): object
    {
        $entite = new Entite();
        $entite->setId($data['id_e'] ?? null);
        $entite->denomination = $data['denomination'];
        $entite->siren = $data['siren'];
        $entite->type = $data['type'];
        $entite->entite_mere = $data['entite_mere'];
        $entite->setEntiteFille($data['entite_fille'] ?? null);
        return $entite;
    }

    public function transform(object $object): array
    {
        $array = [];
        foreach (get_object_vars($object) as $publicProperty => $value) {
            $array[$publicProperty] = $value;
        }
        return $array;
    }
}
