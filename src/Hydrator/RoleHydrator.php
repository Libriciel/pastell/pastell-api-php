<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\Role;

class RoleHydrator implements Hydrator
{
    public function hydrate(array $data): object
    {
        $role = new Role();
        $role->role = $data['role'];
        $role->libelle = $data['libelle'];
        return $role;
    }

    public function transform(object $object): array
    {
        throw new \BadMethodCallException('Not implemented yet');
    }
}
