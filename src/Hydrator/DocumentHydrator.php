<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\Document;

class DocumentHydrator implements Hydrator
{
    /**
     * @return Document
     */
    public function hydrate(array $data): object
    {
        $document = new Document($data['type']);
        $document
            ->setIdD($data['id_d'])
            ->setIdE($data['id_e'])
            ->setLastAction($data['last_action'])
            ->setLastActionDate($data['last_action_date'])
            ->setCreation($data['creation'])
            ->setModification($data['modification'])
        ;

        return $document;
    }

    public function hydrateFromDocumentDetail(array $data, int $id_e): Document
    {
        $type = $data['info']['type'];
        $document = new Document($type);
        $document
            ->setIdD($data['info']['id_d'])
            ->setIdE($id_e)
            ->setLastAction($data['last_action']['action'])
            ->setLastActionDate($data['last_action']['date'])
            ->setCreation($data['info']['creation'])
            ->setModification($data['info']['modification'])
        ;

        $document->documentData = $this->getDocumentDataHydrator($type)->hydrate($data['data']);

        return $document;
    }

    /**
     * @param Document $object
     */
    public function transform(object $object): array
    {
        return $this->getDocumentDataHydrator($object->getType())->transform($object->documentData);
    }

    private function getDocumentDataHydrator(string $type): Hydrator
    {
        if (array_key_exists($type, $this->getCustomHydrators())) {
            $hydrator = $this->getCustomHydrators()[$type];
            return new $hydrator();
        }
        return new Hydrator\Document\DefaultDocumentHydrator();
    }

    public function getCustomHydrators(): array
    {
        return [
            'mailsec' => Hydrator\Document\MailsecDocumentHydrator::class
        ];
    }
}
