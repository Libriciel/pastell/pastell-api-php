<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\UserRole;

class UserRoleHydrator implements Hydrator
{
    public function hydrate(array $data): object
    {
        $userRole = new UserRole();
        $userRole->id_u = $data['id_u'];
        $userRole->id_e = $data['id_e'];
        $userRole->role = $data['role'];
        $userRole->setPermissions($data['droits']);
        return $userRole;
    }

    public function transform(object $object): array
    {
        $array = [];
        foreach (get_object_vars($object) as $publicProperty => $value) {
            $array[$publicProperty] = $value;
        }
        return $array;
    }
}
