<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\User;

class UserHydrator implements Hydrator
{
    /**
     * @param array $data
     * @return User
     */
    public function hydrate(array $data): object
    {
        $user = new User();
        $user->setId($data['id_u'] ?? null);
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->prenom = $data['prenom'] ?? null;
        $user->nom = $data['nom'] ?? null;
        $user->password = $data['password'] ?? null;
        $user->id_e = $data['id_e'] ?? null;
        $user->setCertificate($data['certificat'] ?? null);
        return $user;
    }

    public function transform(object $object): array
    {
        $array = [];
        foreach (get_object_vars($object) as $publicProperty => $value) {
            $array[$publicProperty] = $value;
        }
        if (empty($array['password'])) {
            unset($array['password']);
        }
        return $array;
    }
}
