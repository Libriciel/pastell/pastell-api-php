<?php

namespace PastellClient\Hydrator;

use PastellClient\Hydrator;
use PastellClient\Model\Connector;

class ConnectorHydrator implements Hydrator
{
    /**
     * @return Connector
     */
    public function hydrate(array $data): object
    {
        $type = $data['id_connecteur'];
        $connector = new Connector($type, $data['libelle']);
        $connector
            ->setIdCe($data['id_ce'])
            ->setIdE($data['id_e'])
            ->setType($data['type'])
            ->setFrequenceEnMinute($data['frequence_en_minute'])
            ->setIdVerrou($data['id_verrou'])
        ;

        if (isset($data['data'])) {
            $connector->data = $this->getConnectorDataHydrator($type)->hydrate($data['data']);
        }

        return $connector;
    }

    /**
     * @param Connector $object
     */
    public function transform(object $object): array
    {
        return $this->getConnectorDataHydrator($object->getType())->transform($object->data);
    }

    private function getConnectorDataHydrator(string $type): Hydrator
    {
        if (array_key_exists($type, $this->getCustomHydrators())) {
            $hydrator = $this->getCustomHydrators()[$type];
            return new $hydrator();
        }
        return new Hydrator\Connector\DefaultConnectorHydrator();
    }

    public function getCustomHydrators(): array
    {
        return [];
    }
}
