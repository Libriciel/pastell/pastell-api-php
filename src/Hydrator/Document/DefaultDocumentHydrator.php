<?php

namespace PastellClient\Hydrator\Document;

use PastellClient\Hydrator;
use PastellClient\Model\Document\Data\DefaultDocumentData;

class DefaultDocumentHydrator implements Hydrator
{
    public function hydrate(array $data): object
    {
        $document = new DefaultDocumentData();
        $document->data = $data;

        return $document;
    }


    public function transform(object $object): array
    {
        /** @var DefaultDocumentData $object */
        return $object->data;
    }
}
