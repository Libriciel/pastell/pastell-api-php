<?php

namespace PastellClient\Hydrator\Document;

use PastellClient\Hydrator;
use PastellClient\Model\Document\Data\MailsecDocumentData;

class MailsecDocumentHydrator implements Hydrator
{
    public function hydrate(array $data): object
    {
        $mailsecDocument = new MailsecDocumentData();
        $mailsecDocument->to = $data['to'] ?? null;
        $mailsecDocument->cc = $data['cc'] ?? null;
        $mailsecDocument->bcc = $data['bcc'] ?? null;
        $mailsecDocument->objet = $data['objet'] ?? null;
        $mailsecDocument->message = $data['message'] ?? null;
        return $mailsecDocument;
    }

    /**
     * @param MailsecDocumentData $object
     */
    public function transform(object $object): array
    {
        $array = [];
        foreach (get_object_vars($object) as $publicProperty => $value) {
            $array[$publicProperty] = $value;
        }
        if ($object->getPassword() !== null) {
            $array['password'] = $object->getPassword();
            $array['password2'] = $object->getPassword();
        }
        return $array;
    }
}
