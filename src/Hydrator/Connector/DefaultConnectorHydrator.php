<?php

namespace PastellClient\Hydrator\Connector;

use PastellClient\Hydrator;
use PastellClient\Model\Connector\Data\DefaultConnectorData;

class DefaultConnectorHydrator implements Hydrator
{
    public function hydrate(array $data): object
    {
        $connector = new DefaultConnectorData();
        $connector->data = $data;

        return $connector;
    }

    public function transform(object $object): array
    {
        /** @var DefaultConnectorData $object */
        return $object->data;
    }
}
