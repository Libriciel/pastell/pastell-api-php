<?php

namespace PastellClient\Model;

class UserRole
{
    /** @var int $id_u */
    public $id_u;

    /** @var string $role */
    public $role;

    /** @var int $id_e */
    public $id_e;

    /** @var string[] $droits */
    private $droits;

    /**
     * @param string[] $permissions
     * @return UserRole
     */
    public function setPermissions(array $permissions): UserRole
    {
        $this->droits = $permissions;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getPermissions(): array
    {
        return $this->droits;
    }
}
