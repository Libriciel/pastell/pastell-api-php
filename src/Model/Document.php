<?php

namespace PastellClient\Model;

use DateTime;
use PastellClient\Model\Document\CustomDocumentData;

class Document
{
    public const DATE_FORMAT = 'Y-m-d H:i:s';

    /** @var string */
    private $id_d;

    /** @var int */
    private $id_e;

    /** @var string */
    private $type;

    /** @var string */
    private $last_action;

    /** @var DateTime */
    private $last_action_date;

    /** @var DateTime */
    private $creation;

    /** @var DateTime */
    private $modification;

    /** @var CustomDocumentData */
    public $documentData;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getIdD(): string
    {
        return $this->id_d;
    }

    /**
     * @param string $id_d
     * @return Document
     */
    public function setIdD(string $id_d): Document
    {
        $this->id_d = $id_d;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdE(): int
    {
        return $this->id_e;
    }

    /**
     * @param int $id_e
     * @return Document
     */
    public function setIdE(int $id_e): Document
    {
        $this->id_e = $id_e;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastAction(): string
    {
        return $this->last_action;
    }

    /**
     * @param string $last_action
     * @return Document
     */
    public function setLastAction(string $last_action): Document
    {
        $this->last_action = $last_action;
        return $this;
    }

    public function getLastActionDate(): DateTime
    {
        return $this->last_action_date;
    }

    /**
     * @param string $last_action_date
     * @return Document
     */
    public function setLastActionDate(string $last_action_date): Document
    {
        $this->last_action_date = DateTime::createFromFormat(self::DATE_FORMAT, $last_action_date);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreation(): DateTime
    {
        return $this->creation;
    }

    /**
     * @param string $creation
     * @return Document
     */
    public function setCreation(string $creation): Document
    {
        $this->creation = DateTime::createFromFormat(self::DATE_FORMAT, $creation);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModification(): DateTime
    {
        return $this->modification;
    }

    /**
     * @param string $modification
     * @return Document
     */
    public function setModification(string $modification): Document
    {
        $this->modification =  DateTime::createFromFormat(self::DATE_FORMAT, $modification);
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
