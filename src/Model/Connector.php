<?php

namespace PastellClient\Model;

use PastellClient\Model\Connector\CustomConnectorData;

class Connector
{
    /** @var int */
    private $id_ce;

    /** @var int */
    private $id_e;

    /** @var string */
    private $id_connecteur;

    /** @var string */
    private $libelle;

    /** @var string */
    private $type;

    /** @var string */
    private $frequence_en_minute;

    /** @var string */
    private $id_verrou;

    /** @var CustomConnectorData */
    public $data;

    public function __construct(string $id_connecteur, string $libelle)
    {
        $this->setIdConnecteur($id_connecteur);
        $this->setLibelle($libelle);
    }

    public function getIdCe(): int
    {
        return $this->id_ce;
    }

    public function setIdCe(int $id_ce): Connector
    {
        $this->id_ce = $id_ce;
        return $this;
    }

    public function getIdE(): int
    {
        return $this->id_e;
    }

    public function setIdE(int $id_e): Connector
    {
        $this->id_e = $id_e;
        return $this;
    }

    public function getIdConnecteur(): string
    {
        return $this->id_connecteur;
    }

    public function setIdConnecteur(string $id_connecteur): Connector
    {
        $this->id_connecteur = $id_connecteur;
        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): Connector
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Connector
    {
        $this->type = $type;
        return $this;
    }

    public function getFrequenceEnMinute(): string
    {
        return $this->frequence_en_minute;
    }

    public function setFrequenceEnMinute(string $frequence_en_minute): Connector
    {
        $this->frequence_en_minute = $frequence_en_minute;
        return $this;
    }

    public function getIdVerrou(): string
    {
        return $this->id_verrou;
    }

    public function setIdVerrou(string $id_verrou): Connector
    {
        $this->id_verrou = $id_verrou;
        return $this;
    }
}
