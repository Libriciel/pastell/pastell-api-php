<?php

namespace PastellClient\Model;

class User
{
    /** @var int $id_u */
    private $id_u;
    /** @var string $login */
    public $login;
    /** @var string $email */
    public $email;
    /** @var string $prenom */
    public $prenom;
    /** @var string $nom */
    public $nom;
    /** @var string $password */
    public $password;
    /** @var int $id_e */
    public $id_e;
    /** @var string $certificat */
    private $certificat;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id_u;
    }

    /**
     * @param int $id_u
     * @return User
     */
    public function setId(?int $id_u): User
    {
        $this->id_u = $id_u;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificate(): string
    {
        return $this->certificat;
    }

    /**
     * @param string $certificat
     * @return User
     */
    public function setCertificate(?string $certificat): User
    {
        $this->certificat = $certificat;
        return $this;
    }
}
