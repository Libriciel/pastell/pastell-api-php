<?php

namespace PastellClient\Model\Document\Data;

use PastellClient\Model\Document\CustomDocumentData;

class DefaultDocumentData implements CustomDocumentData
{
    /** @var array */
    public $data;
}
