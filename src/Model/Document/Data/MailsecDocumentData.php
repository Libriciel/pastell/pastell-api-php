<?php

namespace PastellClient\Model\Document\Data;

use PastellClient\Model\Document\CustomDocumentData;

class MailsecDocumentData implements CustomDocumentData
{
    /** @var string */
    public $to;

    /** @var string */
    public $cc;

    /** @var string */
    public $bcc;

    /** @var string */
    private $password;

    /** @var string */
    public $objet;

    /** @var string */
    public $message;

    public function setPassword(string $password): MailsecDocumentData
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }
}
