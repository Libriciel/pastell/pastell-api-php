<?php

namespace PastellClient\Model;

class Role
{
    /** @var string $role */
    public $role;
    /** @var string $libelle */
    public $libelle;
}
