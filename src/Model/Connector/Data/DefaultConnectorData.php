<?php

namespace PastellClient\Model\Connector\Data;

use PastellClient\Model\Connector\CustomConnectorData;

class DefaultConnectorData implements CustomConnectorData
{
    /** @var array */
    public $data;
}
