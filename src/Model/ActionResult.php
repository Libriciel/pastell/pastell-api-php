<?php

namespace PastellClient\Model;

class ActionResult
{
    /** @var bool */
    public $result;
    /** @var string */
    public $message;

    public function __construct(bool $result, string $message)
    {
        $this->result = $result;
        $this->message = $message;
    }
}
