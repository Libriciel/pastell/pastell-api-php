<?php

namespace PastellClient\Model;

class Entite
{
    /** @var int $id_e */
    private $id_e;
    /** @var string $denomination */
    public $denomination;
    /** @var string $siren */
    public $siren;
    /** @var string $type */
    public $type;
    /** @var int $entite_mere */
    public $entite_mere;
    /** @var array $entite_fille */
    private $entite_fille;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id_e;
    }

    /**
     * @param int $id_e
     * @return Entite
     */
    public function setId(?int $id_e): Entite
    {
        $this->id_e = $id_e;
        return $this;
    }

    /**
     * @return array
     */
    public function getEntiteFille(): array
    {
        return $this->entite_fille;
    }

    /**
     * @param array $entite_fille
     * @return Entite
     */
    public function setEntiteFille(?array $entite_fille): Entite
    {
        $this->entite_fille = $entite_fille;
        return $this;
    }
}
