<?php

namespace PastellClient;

interface Hydrator
{
    public function hydrate(array $data): object;

    public function transform(object $object): array;
}
