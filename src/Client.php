<?php

namespace PastellClient;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\AddPathPlugin;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Exception;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\Authentication\BasicAuth;
use Http\Message\Authentication\Bearer;
use PastellClient\HttpClient\Builder;
use PastellClient\HttpClient\Plugin\PastellExceptionPlugin;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

class Client
{
    public const API_PATH = '/api/v2';

    /**
     * @var Builder
     */
    private $httpClientBuilder;

    public function __construct(string $url = null, Builder $httpClientBuilder = null)
    {
        $this->httpClientBuilder = $httpClientBuilder ?: new Builder();
        $this->getHttpClientBuilder()->addPlugin(new PastellExceptionPlugin());
        if ($url) {
            $this->setUrl($url);
        }
    }

    public static function createWithHttpClient(ClientInterface $httpClient, string $url = null): self
    {
        $builder = new Builder($httpClient);
        return new self($url, $builder);
    }

    public function setUrl(string $url): void
    {
        $this->getHttpClientBuilder()->removePlugin(AddHostPlugin::class);
        $this->getHttpClientBuilder()->removePlugin(AddPathPlugin::class);
        $this->getHttpClientBuilder()
            ->addPlugin(new AddHostPlugin(Psr17FactoryDiscovery::findUrlFactory()->createUri($url)));
        $this->getHttpClientBuilder()
            ->addPlugin(new AddPathPlugin(Psr17FactoryDiscovery::findUrlFactory()->createUri(self::API_PATH)));
    }

    public function authenticate(string $username, string $password): void
    {
        $this->getHttpClientBuilder()->removePlugin(AuthenticationPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(new AuthenticationPlugin(new BasicAuth($username, $password)));
    }

    public function tokenAuthenticate(string $token): void
    {
        $this->getHttpClientBuilder()->removePlugin(AuthenticationPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(new AuthenticationPlugin(new Bearer($token)));
    }

    protected function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->getHttpClientBuilder()->getStreamFactory();
    }

    /**
     * @param string $path
     * @return array
     * @throws ClientExceptionInterface
     */
    public function get(string $path): array
    {
        $response = $this->getHttpClient()->get($path);

        return $this->getContent($response);
    }

    /**
     * @throws Exception
     */
    public function getAsStream(string $path): StreamInterface
    {
        return $this->getHttpClient()->get($path)->getBody();
    }

    /**
     * @param string $path
     * @param array $data
     * @return array
     * @throws ClientExceptionInterface
     */
    public function post(string $path, array $data): array
    {
        $response = $this->getHttpClient()->post(
            $path,
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->getContent($response);
    }

    /**
     * @param string $path
     * @param array $data
     * @return array
     * @throws ClientExceptionInterface
     */
    public function patch(string $path, array $data): array
    {
        $response = $this->getHttpClient()->patch(
            $path,
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->getContent($response);
    }

    /**
     * @param string $path
     * @return array
     * @throws ClientExceptionInterface
     */
    public function delete(string $path): array
    {
        $response = $this->getHttpClient()->delete($path);

        return $this->getContent($response);
    }

    private function getContent(ResponseInterface $response): array
    {
        $body = (string)$response->getBody();
        return json_decode($body, true);
    }

    public function version(): Api\Version
    {
        return new Api\Version($this);
    }
}
