<?php

namespace PastellClient\Api;

use Http\Client\Exception;
use PastellClient\Client;
use PastellClient\Hydrator\DocumentHydrator;
use PastellClient\Model\ActionResult;
use PastellClient\Model\Document;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\StreamInterface;

class DocumentsRequester
{
    public const DOCUMENT_PATH = '/entite/%d/document';
    public const DOCUMENT_FILE_PATH = '/entite/%d/document/%s/file/%s/%d';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var DocumentHydrator
     */
    private $documentHydrator;

    public function __construct(Client $client, DocumentHydrator $documentHydrator = null)
    {
        $this->client = $client;
        $this->setDocumentHydrator($documentHydrator ?? new DocumentHydrator());
    }

    public function setDocumentHydrator(DocumentHydrator $documentHydrator): void
    {
        $this->documentHydrator = $documentHydrator;
    }

    private function getDocumentsPath(int $entityId): string
    {
        return sprintf(self::DOCUMENT_PATH, $entityId);
    }

    private function getDocumentFilePath(int $entityId, string $documentId, string $field, int $fileNumber = 0): string
    {
        return sprintf(self::DOCUMENT_FILE_PATH, $entityId, $documentId, $field, $fileNumber);
    }

    /**
     * @return Document[]
     * @throws ClientExceptionInterface
     */
    public function all(int $entityId): array
    {
        $response = $this->client->get($this->getDocumentsPath($entityId));
        $documents = [];
        foreach ($response as $entite) {
            $documents[] = $this->documentHydrator->hydrate($entite);
        }
        return $documents;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function show(int $entityId, string $id): Document
    {
        $response = $this->client->get($this->getDocumentsPath($entityId) . "/$id");
        return $this->documentHydrator->hydrateFromDocumentDetail($response, $entityId);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function create(int $entityId, string $type): Document
    {
        $response = $this->client->post($this->getDocumentsPath($entityId), ['type' => $type]);
        return $this->documentHydrator->hydrateFromDocumentDetail($response, $entityId);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function update(Document $document): Document
    {
        $response = $this->client->patch(
            $this->getDocumentsPath($document->getIdE()) . '/' . $document->getIdD(),
            $this->documentHydrator->transform($document)
        );
        return $this->documentHydrator->hydrateFromDocumentDetail($response['content'], $document->getIdE());
    }

    /**
     * @throws Exception
     */
    public function getFile(Document $document, string $field, int $fileNumber = 0): StreamInterface
    {
        return $this->client->getAsStream(
            $this->getDocumentFilePath($document->getIdE(), $document->getIdD(), $field, $fileNumber)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function uploadFile(
        Document $document,
        string $field,
        string $pathToFile,
        string $fileName,
        int $fileNumber = 0
    ): Document {
        $response = $this->client->post(
            $this->getDocumentFilePath($document->getIdE(), $document->getIdD(), $field, $fileNumber),
            [
                'file_name' => $fileName,
                'file_content' => file_get_contents($pathToFile)
            ]
        );

        return $this->documentHydrator->hydrateFromDocumentDetail($response['content'], $document->getIdE());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function deleteFile(Document $document, string $field, int $fileNumber = 0): Document
    {
        $response = $this->client->delete(
            $this->getDocumentFilePath($document->getIdE(), $document->getIdD(), $field, $fileNumber)
        );

        return $this->documentHydrator->hydrateFromDocumentDetail($response, $document->getIdE());
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function triggerAction(Document $document, string $action): ActionResult
    {
        $response = $this->client->post(
            $this->getDocumentsPath($document->getIdE()) . sprintf('/%s/action/%s', $document->getIdD(), $action),
            []
        );

        return new ActionResult($response['result'], $response['message']);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getExternalData(Document $document, string $field): array
    {
        return $this->client->get(
            $this->getDocumentsPath($document->getIdE()) . sprintf('/%s/externalData/%s', $document->getIdD(), $field)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function setExternalData(Document $document, string $field, array $data): Document
    {
        $response = $this->client->patch(
            $this->getDocumentsPath($document->getIdE()) . sprintf('/%s/externalData/%s', $document->getIdD(), $field),
            $data
        );

        return $this->documentHydrator->hydrateFromDocumentDetail($response, $document->getIdE());
    }
}
