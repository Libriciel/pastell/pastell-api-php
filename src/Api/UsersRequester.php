<?php

namespace PastellClient\Api;

use PastellClient\Client;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\User;
use Psr\Http\Client\ClientExceptionInterface;

class UsersRequester
{
    public const USER_PATH = '/utilisateur';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    public function __construct(Client $client, UserHydrator $userHydrator = null)
    {
        $this->client = $client;
        $this->setUserHydrator($userHydrator ?? new UserHydrator());
    }

    public function setUserHydrator(UserHydrator $userHydrator): void
    {
        $this->userHydrator = $userHydrator;
    }

    /**
     * @param array $searchOptions
     * @return User[]
     * @throws ClientExceptionInterface
     */
    public function all(array $searchOptions = []): iterable
    {
        $path = self::USER_PATH;
        if (!empty($searchOptions)) {
            $path .= '?' . http_build_query($searchOptions);
        }
        $response = $this->client->get($path);
        $userList = [];
        foreach ($response as $user) {
            $userList[] = $this->userHydrator->hydrate($user);
        }
        return $userList;
    }

    /**
     * @param int $id
     * @return User
     * @throws ClientExceptionInterface
     */
    public function show(int $id): User
    {
        $response = $this->client->get(self::USER_PATH . '/' . $id);
        return $this->userHydrator->hydrate($response);
    }

    /**
     * @param User $user
     * @return User
     * @throws ClientExceptionInterface
     */
    public function create(User $user): User
    {
        $response = $this->client->post(self::USER_PATH, $this->userHydrator->transform($user));
        return $this->userHydrator->hydrate($response);
    }

    /**
     * @param User $user
     * @return User
     * @throws ClientExceptionInterface
     */
    public function update(User $user): User
    {
        $response = $this->client->patch(self::USER_PATH . '/' . $user->getId(), $this->userHydrator->transform($user));
        return $this->userHydrator->hydrate($response);
    }

    /**
     * @param int $id
     * @return array
     * @throws ClientExceptionInterface
     */
    public function remove(int $id): array
    {
        return $this->client->delete(self::USER_PATH . '/' . $id);
    }
}
