<?php

namespace PastellClient\Api;

use PastellClient\Client;
use Psr\Http\Client\ClientExceptionInterface;

class Version
{
    public const VERSION_PATH = '/version';

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @throws ClientExceptionInterface
     */
    public function show(): array
    {
        return $this->client->get(self::VERSION_PATH);
    }
}
