<?php

namespace PastellClient\Api;

use PastellClient\Client;
use Psr\Http\Client\ClientExceptionInterface;

class ModuleAssociationsRequester
{
    /** @var string */
    public const ENTITY_MODULE_PATH = '/entite/%s/flux';

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function all(int $entityId, ?string $module = null, ?string $type = null): array
    {
        $path = $this->getEntityModulePath($entityId);
        $options = [];
        if (isset($module)) {
            $options['flux'] = $module;
        }
        if (isset($type)) {
            $options['type'] = $type;
        }
        if (!empty($options)) {
            $path .= '?' . http_build_query($options);
        }
        return $this->client->get($path);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function associate(
        int $entityId,
        int $connectorId,
        string $module,
        string $type,
        int $numSameType = 0
    ): array {
        return $this->client->post(
            $this->getEntityModulePath($entityId)
            . sprintf('/%s/connecteur/%s', $module, $connectorId),
            [
                'type' => $type,
                'num_same_type' => $numSameType
            ]
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function disassociate(int $entityId, int $associationId): array
    {
        return $this->client->delete($this->getEntityModulePath($entityId) . '?id_fe=' . $associationId);
    }

    /**
     * @param int $entityId
     * @return string
     */
    private function getEntityModulePath(int $entityId): string
    {
        return sprintf(self::ENTITY_MODULE_PATH, $entityId);
    }
}
