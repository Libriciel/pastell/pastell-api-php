<?php

namespace PastellClient\Api;

use Http\Client\Exception;
use PastellClient\Client;
use PastellClient\Hydrator\ConnectorHydrator;
use PastellClient\Model\ActionResult;
use PastellClient\Model\Connector;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\StreamInterface;

class ConnectorsRequester
{
    /** @var string */
    public const ALL_CONNECTOR_PATH = '/connecteur/all';

    /** @var string */
    public const ENTITY_CONNECTOR_PATH = '/entite/%s/connecteur';

    /** @var Client */
    private $client;

    /** @var ConnectorHydrator */
    private $connectorHydrator;

    public function __construct(Client $client, ConnectorHydrator $connectorHydrator = null)
    {
        $this->client = $client;
        $this->setConnectorHydrator($connectorHydrator ?? new ConnectorHydrator());
    }

    public function setConnectorHydrator(ConnectorHydrator $connectorHydrator): void
    {
        $this->connectorHydrator = $connectorHydrator;
    }

    /**
     * @return Connector[]
     * @throws ClientExceptionInterface
     */
    public function all(int $entityId = null): array
    {
        $url = isset($entityId) ? sprintf(self::ENTITY_CONNECTOR_PATH, $entityId) : self::ALL_CONNECTOR_PATH;
        $response = $this->client->get($url);
        $connectors = [];
        foreach ($response as $connector) {
            $connectors[] = $this->connectorHydrator->hydrate($connector);
        }
        return $connectors;
    }

    /**
     * @return Connector[]
     * @throws ClientExceptionInterface
     */
    public function allByType(string $type): array
    {
        $response = $this->client->get(self::ALL_CONNECTOR_PATH . "/$type");
        $connectors = [];
        foreach ($response as $connector) {
            $connectors[] = $this->connectorHydrator->hydrate($connector);
        }
        return $connectors;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function show(int $entityId, int $connectorId): Connector
    {
        $response = $this->client->get(sprintf(self::ENTITY_CONNECTOR_PATH, $entityId) . "/$connectorId");
        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function create(int $entityId, string $type, string $libelle): Connector
    {
        $response = $this->client->post(
            sprintf(self::ENTITY_CONNECTOR_PATH, $entityId),
            [
                'id_connecteur' => $type,
                'libelle' => $libelle
            ]
        );

        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function update(Connector $connector): Connector
    {
        $response = $this->client->patch(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) . '/' . $connector->getIdCe() . '/content',
            $this->connectorHydrator->transform($connector)
        );
        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function changeLabel(Connector $connector): Connector
    {
        $response = $this->client->patch(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) . '/' . $connector->getIdCe(),
            ['libelle' => $connector->getLibelle()]
        );
        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getExternalData(Connector $connector, string $field): array
    {
        return $this->client->get(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) .
            sprintf('/%s/externalData/%s', $connector->getIdCe(), $field)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function setExternalData(Connector $connector, string $field, array $data): Connector
    {
        $response = $this->client->patch(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) .
            sprintf('/%s/externalData/%s', $connector->getIdCe(), $field),
            $data
        );

        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws Exception
     */
    public function getFile(Connector $connector, string $field): StreamInterface
    {
        return $this->client->getAsStream(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) .
            sprintf('/%s/file/%s', $connector->getIdCe(), $field)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function uploadFile(
        Connector $connector,
        string $field,
        string $pathToFile,
        string $fileName
    ): Connector {
        $response = $this->client->post(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) .
            sprintf('/%s/file/%s', $connector->getIdCe(), $field),
            [
                'file_name' => $fileName,
                'file_content' => file_get_contents($pathToFile)
            ]
        );

        return $this->connectorHydrator->hydrate($response);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function triggerAction(Connector $connector, string $action): ActionResult
    {
        $response = $this->client->post(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) .
            sprintf('/%s/action/%s', $connector->getIdCe(), $action),
            []
        );
        return new ActionResult($response['result'], $response['last_message']);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete(Connector $connector): bool
    {
        $response = $this->client->delete(
            sprintf(self::ENTITY_CONNECTOR_PATH, $connector->getIdE()) . "/" . $connector->getIdCe()
        );

        return isset($response['result']) && $response['result'] === 'ok';
    }
}
