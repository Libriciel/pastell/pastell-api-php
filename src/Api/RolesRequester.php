<?php

namespace PastellClient\Api;

use PastellClient\Client;
use PastellClient\Hydrator\RoleHydrator;
use PastellClient\Model\Role;
use Psr\Http\Client\ClientExceptionInterface;

class RolesRequester
{
    public const ROLE_PATH = '/role';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var RoleHydrator
     */
    private $roleHydrator;

    public function __construct(Client $client, RoleHydrator $roleHydrator = null)
    {
        $this->client = $client;
        $this->setRoleHydrator($roleHydrator ?? new RoleHydrator());
    }

    public function setRoleHydrator(RoleHydrator $roleHydrator): void
    {
        $this->roleHydrator = $roleHydrator;
    }

    /**
     * @return Role[]
     * @throws ClientExceptionInterface
     */
    public function all(): iterable
    {
        $response = $this->client->get(self::ROLE_PATH);
        $roleList = [];
        foreach ($response as $role) {
            $roleList[] = $this->roleHydrator->hydrate($role);
        }
        return $roleList;
    }
}
