<?php

namespace PastellClient\Api;

use PastellClient\Client;
use PastellClient\Hydrator\UserRoleHydrator;
use PastellClient\Model\UserRole;
use Psr\Http\Client\ClientExceptionInterface;

class UserRolesRequester
{
    public const USER_ROLES_PATH = '/utilisateur/%s/role';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var UserRoleHydrator
     */
    private $userRoleHydrator;

    public function __construct(Client $client, UserRoleHydrator $userRoleHydrator = null)
    {
        $this->client = $client;
        $this->setUserRoleHydrator($userRoleHydrator ?? new UserRoleHydrator());
    }


    public function setUserRoleHydrator(UserRoleHydrator $userRoleHydrator): void
    {
        $this->userRoleHydrator = $userRoleHydrator;
    }

    /**
     * @param int $id_u
     * @return UserRole[]
     * @throws ClientExceptionInterface
     */
    public function all(int $id_u): iterable
    {
        $response = $this->client->get($this->getUserRolesPath($id_u));
        $userRoles = [];
        foreach ($response as $userRole) {
            $userRoles[] = $this->userRoleHydrator->hydrate($userRole);
        }
        return $userRoles;
    }

    /**
     * @param UserRole $userRole
     * @return array
     * @throws ClientExceptionInterface
     */
    public function add(UserRole $userRole): array
    {
        return $this->client->post(
            $this->getUserRolesPath($userRole->id_u),
            $this->userRoleHydrator->transform($userRole)
        );
    }

    /**
     * @param UserRole $userRole
     * @return array
     * @throws ClientExceptionInterface
     */
    public function remove(UserRole $userRole): array
    {
        return $this->client->delete(
            $this->getUserRolesPath($userRole->id_u) . '?' .
            http_build_query($this->userRoleHydrator->transform($userRole))
        );
    }

    /**
     * @param int $id_u
     * @return string
     */
    private function getUserRolesPath(int $id_u): string
    {
        return sprintf(self::USER_ROLES_PATH, $id_u);
    }
}
