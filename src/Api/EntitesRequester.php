<?php

namespace PastellClient\Api;

use PastellClient\Client;
use PastellClient\Hydrator\EntiteHydrator;
use PastellClient\Model\Entite;
use Psr\Http\Client\ClientExceptionInterface;

class EntitesRequester
{
    public const ENTITE_PATH = '/entite';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;

    public function __construct(Client $client, EntiteHydrator $entiteHydrator = null)
    {
        $this->client = $client;
        $this->setEntiteHydrator($entiteHydrator ?? new EntiteHydrator());
    }


    public function setEntiteHydrator(EntiteHydrator $entiteHydrator): void
    {
        $this->entiteHydrator = $entiteHydrator;
    }

    /**
     * @return Entite[]
     * @throws ClientExceptionInterface
     */
    public function all(): array
    {
        $response = $this->client->get(self::ENTITE_PATH);
        $entiteList = [];
        foreach ($response as $entite) {
            $entiteList[] = $this->entiteHydrator->hydrate($entite);
        }
        return $entiteList;
    }

    /**
     * @param int $id
     * @return Entite
     * @throws ClientExceptionInterface
     */
    public function show(int $id): Entite
    {
        $response = $this->client->get(self::ENTITE_PATH . '/' . $id);
        return $this->entiteHydrator->hydrate($response);
    }

    /**
     * @param Entite $entite
     * @return Entite
     * @throws ClientExceptionInterface
     */
    public function create(Entite $entite): Entite
    {
        $response = $this->client->post(self::ENTITE_PATH, $this->entiteHydrator->transform($entite));
        return $this->entiteHydrator->hydrate($response);
    }

    /**
     * @param Entite $entite
     * @return Entite
     * @throws ClientExceptionInterface
     */
    public function update(Entite $entite): Entite
    {
        $response = $this->client->patch(
            self::ENTITE_PATH . '/' . $entite->getId(),
            $this->entiteHydrator->transform($entite)
        );
        return $this->entiteHydrator->hydrate($response);
    }

    /**
     * @param int $id
     * @return array
     * @throws ClientExceptionInterface
     */
    public function remove(int $id): array
    {
        return $this->client->delete(self::ENTITE_PATH . '/' . $id);
    }
}
