# [1.2.0] - 2023-12-12

* Ajout de l'authentification par token pour Pastell v4 #3

# [1.1.0] - 2021-06-15

* Ajout des APIs de gestion des associations de connecteurs avec les types de dossier #2
